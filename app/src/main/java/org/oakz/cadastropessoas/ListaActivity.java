package org.oakz.cadastropessoas;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;
import java.util.ArrayList;
import services.CadastroAdapter;
import services.Cadastros;
import services.CadastrosDAO;

public class ListaActivity extends AppCompatActivity {

    private ListView listaCadastrados;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);

        ArrayList<Cadastros> cadastros = new ArrayList<>();

        Cursor cursor = new CadastrosDAO(getBaseContext()).listar();
        if(cursor != null){
            do{
                String nome = cursor.getString(1);
                cadastros.add(new Cadastros(cursor.getString(1), cursor.getString(2), cursor.getString(3)));
            } while (cursor.moveToNext());
        }

        listaCadastrados = (ListView) findViewById(R.id.listaCadastrados);
        CadastroAdapter adapter = new CadastroAdapter(cadastros, this);
        listaCadastrados.setAdapter(adapter);
    }

    @Override
    protected void onRestart() {

        ArrayList<Cadastros> cadastros = new ArrayList<>();

        Cursor cursor = new CadastrosDAO(getBaseContext()).listar();
        if(cursor != null){
            do{
                String nome = cursor.getString(1);
                cadastros.add(new Cadastros(cursor.getString(1), cursor.getString(2), cursor.getString(3)));
            } while (cursor.moveToNext());
        }

        listaCadastrados = (ListView) findViewById(R.id.listaCadastrados);
        CadastroAdapter adapter = new CadastroAdapter(cadastros, this);
        listaCadastrados.setAdapter(adapter);

        super.onRestart();
    }
}
