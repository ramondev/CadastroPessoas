package org.oakz.cadastropessoas;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import services.Cadastros;
import services.CadastrosDAO;

public class MainActivity extends AppCompatActivity {

    public ImageView foto;
    public Uri caminhoArquivo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button salvar = (Button) findViewById(R.id.salvar);

        salvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText nome = (EditText) findViewById(R.id.nome);
                EditText email = (EditText) findViewById(R.id.email);
                /*ImageView foto = (ImageView) findViewById(R.id.foto);

                Bitmap bm = BitmapFactory.decodeFile(caminhoArquivo.toString());
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] b = baos.toByteArray();

                String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
                */
                final Cadastros cadastro = new Cadastros(
                        nome.getText().toString(),
                        email.getText().toString(),
                        //encodedImage
                        caminhoArquivo.toString()
                );

                System.out.println(caminhoArquivo);

                String text = new CadastrosDAO(getBaseContext()).adicionar(cadastro);

                nome.setText("");
                email.setText("");
                foto.setImageResource(R.drawable.stormtrooper);

                Toast.makeText(MainActivity.this, text, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public void picPhoto(View view){

        foto = (ImageView) findViewById(R.id.foto);

        Intent irParaCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        String caminhoFoto = Environment.DIRECTORY_PICTURES + "/AIVIDADE_" + System.currentTimeMillis() + ".png";
        File arquivo = Environment.getExternalStoragePublicDirectory(caminhoFoto);
        MainActivity.this.caminhoArquivo = Uri.fromFile(arquivo);
        irParaCamera.putExtra(MediaStore.EXTRA_OUTPUT, MainActivity.this.caminhoArquivo);
        startActivityForResult(irParaCamera, 13);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 13:
                if (resultCode == Activity.RESULT_OK){
                    Bitmap imagem_bitmap = BitmapFactory.decodeFile(caminhoArquivo.getPath());
                    // cortando a imagem para ela não ficar com aparência deformada (Tem como fazer isso usando algum métido tipo o setScaleType?
                    Bitmap imagem_tratada;

                    if (imagem_bitmap.getWidth() >= imagem_bitmap.getHeight()){

                        imagem_tratada = Bitmap.createBitmap(
                                imagem_bitmap,
                                imagem_bitmap.getWidth()/2 - imagem_bitmap.getHeight()/2,
                                0,
                                imagem_bitmap.getHeight(),
                                imagem_bitmap.getHeight()
                        );

                    }else{

                        imagem_tratada = Bitmap.createBitmap(
                                imagem_bitmap,
                                0,
                                imagem_bitmap.getHeight()/2 - imagem_bitmap.getWidth()/2,
                                imagem_bitmap.getWidth(),
                                imagem_bitmap.getWidth()
                        );
                    }

                    Bitmap img_final = Bitmap.createScaledBitmap(imagem_tratada, 512, 512, true);

                    foto.setImageBitmap(img_final);
                }
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.listar:
                Intent listar = new Intent(MainActivity.this, ListaActivity.class);
                /*
                String img = caminhoArquivo.toString();
                listar.putExtra("uri_img", img);
                */
                startActivity(listar);
            break;
        }
        return super.onOptionsItemSelected(item);
    }
}
