package services;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DAO extends SQLiteOpenHelper {

    private static int VERSION = 1;
    private static String DATABASE = "dbCadastro.db";
    public static String CADASTROS = "CADASTROS";

    public DAO(Context context) {
        super(context, DATABASE, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE " + CADASTROS + "(" +
                "ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "NOME TEXT NOT NULL, " +
                "EMAIL TEXT NOT NULL, " +
                "FOTO TEXT NOT NULL" +
                ")";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql = "CREATE TABLE " + CADASTROS + "(" +
                "ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "NOME TEXT NOT NULL, " +
                "EMAIL TEXT NOT NULL, " +
                "FOTO TEXT NOT NULL" +
                ")";
        db.execSQL(sql);
    }
}
