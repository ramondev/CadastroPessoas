package services;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class CadastrosDAO {
    private SQLiteDatabase db;
    private Context context;

    public CadastrosDAO(Context context) {
        this.context = context;
    }

    public String adicionar(Cadastros cadastro){

        db = new DAO(context).getWritableDatabase();

        ContentValues valores = new ContentValues();
        valores.put("NOME", cadastro.getNome());
        valores.put("EMAIL",cadastro.getEmail());
        valores.put("FOTO",cadastro.getFoto());
        long result = db.insert(DAO.CADASTROS, null, valores);
        String retorno;
        if(result != -1){
            retorno = "Cadastro efetuado com sucesso!";
        } else {
            retorno = "Não foi possível efetuar este cadastro.";
        }
        db.close();

        return retorno;
    }

    public Cursor listar(){

        db = new DAO(context).getReadableDatabase();
        String[] args = {};
        Cursor cursor = db.rawQuery("select * from CADASTROS", args);
        if (cursor != null){
            cursor.moveToFirst();
        }
        db.close();
        return cursor;
    }
}
