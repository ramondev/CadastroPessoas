package services;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.oakz.cadastropessoas.R;

import java.util.ArrayList;

public class CadastroAdapter extends BaseAdapter {

    private ArrayList<Cadastros> cadastros;
    Context context;

    public CadastroAdapter(ArrayList<Cadastros> cadastros, Context context) {
        this.cadastros = cadastros;
        this.context = context;
    }

    @Override
    public int getCount() {
        return cadastros.size();
    }

    @Override
    public Object getItem(int position) {
        return cadastros.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Cadastros cadastro = cadastros.get(position);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View item = inflater.inflate(R.layout.item_cadastrado, null);

        TextView nome = (TextView) item.findViewById(R.id.itemNome);
        TextView email = (TextView) item.findViewById(R.id.itemEmail);
        ImageView foto = (ImageView) item.findViewById(R.id.itemFoto);

        Uri uriImg = Uri.parse(cadastro.getFoto());
        String imgLink = uriImg.getPath();
        /*
        byte[] data = Base64.decode(cadastro.getFoto(), Base64.DEFAULT);
        Bitmap img = BitmapFactory.decodeByteArray(data , 0, data .length);
        //Bitmap imagem_bitmap = BitmapFactory.decodeFile(cadastro.getFoto());
        Bitmap imagem_tratada = Bitmap.createScaledBitmap(img, 150, 150, true);
        */

        Bitmap imagem_tratada;

        Bitmap imagem_bitmap = BitmapFactory.decodeFile(imgLink);

        if (imagem_bitmap.getWidth() >= imagem_bitmap.getHeight()){

            imagem_tratada = Bitmap.createBitmap(
                    imagem_bitmap,
                    imagem_bitmap.getWidth()/2 - imagem_bitmap.getHeight()/2,
                    0,
                    imagem_bitmap.getHeight(),
                    imagem_bitmap.getHeight()
            );

        }else{

            imagem_tratada = Bitmap.createBitmap(
                    imagem_bitmap,
                    0,
                    imagem_bitmap.getHeight()/2 - imagem_bitmap.getWidth()/2,
                    imagem_bitmap.getWidth(),
                    imagem_bitmap.getWidth()
            );
        }

        Bitmap imagem_final = Bitmap.createScaledBitmap(imagem_tratada, 100, 100, true);

        System.out.println("imagem_tratada: "+ imagem_final);

        nome.setText(cadastro.getNome());
        email.setText(cadastro.getEmail());
        //foto.setImageURI(Uri.parse(cadastro.getFoto()));
        foto.setImageBitmap(imagem_final);

        return item;


    }
}
